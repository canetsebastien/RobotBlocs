RobotBlocs
===================
tangible programmation of drawing robot, by kids for kids.

[Click here to launch video: https://mediacad.ac-nantes.fr/m/2332](https://mediacad.ac-nantes.fr/m/2332)
![](https://raw.githubusercontent.com/technologiescollege/RobotBlocs/master/vignette.jpg)


**==>**[**Animation à ouvrir.**](https://github.com/technologiescollege/RobotBlocs/blob/master/principe.swf?raw=true)

Designers & developpers
----------
Chaque version du projet, chaque année, chaque groupe et chaque organisation a été différente.
Vous trouverez les noms des élèves, quand ils ont bien voulu qu'on les laisse, dans les dossiers (électronique ou Modeles_3D\Solidworks) de chaque année.

Each version of the project, each year, each group and each organisation has been different.
You will find the names of the students, when they were willing to be left, in the folders (electronic or Modeles_3D\Solidworks) of each year.

Explanation/explication
----------
![enter image description here](https://raw.githubusercontent.com/technologiescollege/RobotBlocs/master/principe.jpg)

Les enfants jouent naturellement avec leurs mains et apprennent avec dès leurs premières années, construisant toute sorte de choses qui peuvent les amuser mais aussi en faisant ces choses ensemble. Un des avantages de la programmation tangible est qu'il rend l'idée du programme physique, afin que les enfants puissent jouer avec, le manipuler et refaire autant que voulu avant de le téléverser. La tranche d'âge visée est celle d'enfants entre 6 et 7 ans. 

Notre problématique est la suivante : "concevoir un système permettant à des enfants âgés de 6 ans ou plus, sachant lire ou pas, avec des difficultés motrices ou pas, d'apprendre les bases de la programmation et de l’algorithmique". 

A la suite de la lecture du cahier des charges fonctionnels, nous pouvons donc affirmer que nous devons concevoir un système composé de 3 sous-systèmes : 

 - un bloc maître qui transmet les informations des blocs programmes à un robot dessinateur.  
 - des blocs programmes qui auront une fonction, liée aux mouvements du robot,définie par un couvercle amovible ainsi qu'une capacité à s'emboîter entre eux afin de former une chaîne pour transmettre l'information. 
 - un robot dessinateur motorisé autonome en énergie qui possédera une partie mobile pour déplacer un crayon, d'un diamètre de 8mm à 15mm, afin de tracer des formes sur papier. Il exécutera les commandes des blocs programmes envoyées via le bloc maître. 

